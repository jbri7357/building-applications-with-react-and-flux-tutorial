"use strict";

var Dispatcher = require('../dispatcher/app_dispatcher');
var ActionTypes = require('../constants/action_types');
var AuthorApi = require('../api/author_api');
var CourseApi = require('../api/course_api');

var InitializeActions = {
  initApp: function() {
    Dispatcher.dispatch({
      actionType: ActionTypes.INITIALIZE,
      initialData: {
        authors: AuthorApi.getAllAuthors(),
        courses: CourseApi.getAllCourses()
      }
    });
  }
};

module.exports = InitializeActions;
