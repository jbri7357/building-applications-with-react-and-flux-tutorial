"use strict";

var Dispatcher = require('../dispatcher/app_dispatcher');
var CourseApi = require('../api/course_api');
var ActionTypes = require('../constants/action_types');

var CourseActions = {
  createCourse: function(course) {
    var newCourse = CourseApi.saveCourse(course);

    // Hey dispatcher, go tell all the stores that a course was just created.
    Dispatcher.dispatch({
      actionType: ActionTypes.CREATE_COURSE,
      course: newCourse
    });
  },

  updateCourse: function(course) {
    var updatedCourse = CourseApi.saveCourse(course);

    Dispatcher.dispatch({
      actionType: ActionTypes.UPDATE_COURSE,
      course: updatedCourse
    });
  },

  deleteCourse: function(id) {
    CourseApi.deleteCourse(id);

    Dispatcher.dispatch({
      actionType: ActionTypes.DELETE_COURSE,
      id: id
    });
  }
};
module.exports = CourseActions;
