"use strict";

var React = require('react');

var Router = require('react-router');
var DefaultRoute = Router.DefaultRoute;
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var Redirect = Router.Redirect;

var routes = (
  <Route name="app" path="/" handler={require('./components/app.jsx')}>
    <DefaultRoute handler={require('./components/home_page.jsx')}/>

    <Route name="authors" handler={require('./components/authors/author_page.jsx')}/>
    <Route name="addAuthor" path="author" handler={require('./components/authors/manage_author_page.jsx')}/>
    <Route name="manageAuthor" path="author/:id" handler={require('./components/authors/manage_author_page.jsx')}/>

    <Route name="courses" handler={require('./components/courses/course_page.jsx')}/>
    <Route name="addCourse" path="course" handler={require('./components/courses/manage_course_page.jsx')}/>
    <Route name="manageCourse" path="course/:id" handler={require('./components/courses/manage_course_page.jsx')}/>

    <Route name="about" handler={require('./components/about/about_page.jsx')}/>

    <NotFoundRoute handler={require('./components/not_found_page.jsx')}/>

    <Redirect from="about-us" to="about"/>
    <Redirect from="awthurs" to="authors"/>
    <Redirect from="about/*" to="about"/>
  </Route>
);

module.exports = routes;
